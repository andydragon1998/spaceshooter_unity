﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BackgroundScroll : MonoBehaviour 
{

	public GameObject asteroid = new GameObject();

	int x = 0;

	int refreshRate = 60;
	int rateUp = 5;
	int MaxRate = 135;


	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		x++;
		/*background.transform.position += new Vector3 (0, -0.4f, 0);*/

		int Dir = Random.Range (-8, 1);

		if (x > 140) 
		{
			Instantiate(asteroid,new Vector3(Dir, 13, 0),Quaternion.identity);

			if (refreshRate < MaxRate) 
			{
				refreshRate += rateUp;
			} 

			x = refreshRate;


		}

		/*if (background.transform.position.y < 0) 
		{
			SceneManager.LoadScene("Data/Scenes/BossBattle");
		}*/

	}
}
