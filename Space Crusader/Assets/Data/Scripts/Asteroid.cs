﻿using UnityEngine;
using System.Collections;

public class Asteroid : MonoBehaviour 
{

	public float astySpeed = -0.17f;

	public Collider hitbox;

	//public GameObject Scripty;


	int Dir;
	// Use this for initialization
	void Start () 
	{
		Dir = Random.Range (1, 100);

		if (Dir < 33) 
		{
			Dir = 1; 
		} 
		else if (Dir < 66) 
		{
			Dir = 3; 
		} 
		else
		{
			Dir = 2; 
		}

		hitbox = GetComponent<Collider>();

		//Scripty = GameObject.FindGameObjectWithTag ("PlayerScripter");
	}
	
	// Update is called once per frame
	void Update () 
	{
		

		switch (Dir) 
		{
			case 1:
				transform.position += new Vector3 (-astySpeed, astySpeed, 0);
				//transform.rotation += new Vector3 (-astySpeed, astySpeed, 0);
			break;
			case 2:
				transform.position += new Vector3 (astySpeed, astySpeed, 0);
				//transform.rotation += new Vector3 (astySpeed, astySpeed, 0);
			break;
			case 3: 
				transform.position += new Vector3 (0, astySpeed, 0);
				//transform.rotation += new Vector3 (0, astySpeed, 0);
			break;
		}
			
		if (transform.position.y < -2) 
		{
			Destroy(gameObject);
		}

		//Collision check and kill object
		if (hitbox.bounds.Contains(GameObject.FindGameObjectWithTag("Player").transform.position)) 
		{
			//GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController> ().playerHP -= GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController> ().PLAYER_DAMAGE_ASTEROID;
			Destroy(gameObject);

		}


		/*try
		{
			if (hitbox.bounds.Contains(Scripty.GetComponent<PlayerController>().bulletList.FindLast<PlayerBullet>().transform.position))
			{
				Destroy(gameObject);
			}
		}
		catch {

		}*/

	}
}
