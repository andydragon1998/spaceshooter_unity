﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

public class GameOver : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (CrossPlatformInputManager.GetButtonDown("Jump")) 
		{
			SceneManager.LoadScene("Data/Scenes/TitleStage");
		}
	}
}
