﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour 
{

	public GameObject player;

	public Collider playerHitbox;

	public GameObject HealthBar;

	public GameObject HeatBar;

	public GameObject bullet = new GameObject();

	public GameObject AIAssitanty;

	public Sprite AIAssistantAmazed = new Sprite();
	public Sprite AIAssistantAmmused = new Sprite();
	public Sprite AIAssistantCuriosity = new Sprite();
	public Sprite AIAssistantDesperate = new Sprite();
	public Sprite AIAssistantDetermination = new Sprite();
	public Sprite AIAssistantDispleased = new Sprite();
	public Sprite AIAssistantWorry = new Sprite();

	public int PLAYER_MAX_HEALTH = 300;
	public int PLAYER_MAX_HEAT_LEVEL = 300;
	public int PLAYER_HEAT_LEVEL_ADDER = 35;
	public int PLAYER_HEAT_LEVEL_REDUCTOR = 1;
	public int PLAYER_DAMAGE_ASTEROID = 5;
	public int PLAYER_DAMAGE_BULLET = 10;
	public int PLAYER_DAMAGE_SPACESHIP = 25;


	public int playerHP = 300;
	public int playerHeat= 0;
	public float playerX;
	public float playerY;
	public float playerZ;

	bool isFireKeyDown;

	SpriteRenderer sr;

	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("Player");
		HealthBar = GameObject.FindGameObjectWithTag("PlayerHealthBar");
		HeatBar = GameObject.FindGameObjectWithTag("PlayerHeatBar");
		AIAssitanty = GameObject.FindGameObjectWithTag("AI Assistant");
		//playerHitbox = player.GetComponent<Collider> ();
		sr = AIAssitanty.GetComponent<SpriteRenderer>();

		playerX = player.transform.position.x;
		playerY = player.transform.position.y;
	}

	void Update()
	{
		float horizontal = CrossPlatformInputManager.GetAxis("Horizontal") / 10;
		float vertical = CrossPlatformInputManager.GetAxis("Vertical") / 10;



		playerX = player.transform.position.x;
		playerY = player.transform.position.y;
		playerZ = player.transform.position.z;

		if (playerX < -7) 
		{
			player.transform.position += new Vector3 (horizontal +1, vertical, 0);
		} 
		else if (playerX > 2) 
		{
			player.transform.position += new Vector3 (horizontal -1, vertical, 0);
		}
		else
		{
			player.transform.position += new Vector3 (horizontal, vertical, 0);
		}

		if (playerY > 7) 
		{
			player.transform.position += new Vector3 (horizontal, vertical  -1, 0);
		} 
		else if (playerY < 0) 
		{
			player.transform.position += new Vector3 (horizontal, vertical+1, 0);
		}

		if (CrossPlatformInputManager.GetButtonDown("Jump") && !isFireKeyDown) 
		{
			Instantiate(bullet,new Vector3(playerX, playerY, playerZ),Quaternion.identity);
			isFireKeyDown = true;
			playerHeat += PLAYER_HEAT_LEVEL_ADDER;
		} 
		else 
		{
			isFireKeyDown = false;
		}

		UpdateHeatAndHealtHUD ();

		AIController ();
	}


	public void UpdateHeatAndHealtHUD()
	{
		HeatBar.transform.localScale = new Vector3 (1.5f, ((float)playerHeat / 100.0f), 1);
		HealthBar.transform.localScale = new Vector3 (1.5f, ((float)playerHP / 100.0f), 1);

		if (playerHeat > PLAYER_MAX_HEAT_LEVEL || playerHP < 0) 
		{
			SceneManager.LoadScene("Data/Scenes/GameOver");
		}

		playerHeat -= 1;

		if (playerHeat < 0) 
		{
			playerHeat = 0;
		}

		/*if(playerHitbox.bounds.Contains(GameObject.FindWithTag("Asteroid").transform.position))
		{
			playerHP -= PLAYER_DAMAGE_ASTEROID;
		}*/

	}

	void AIController()
	{
		if(playerHeat > 60)
			sr.sprite = AIAssistantCuriosity;
		if(playerHeat > 90)
			sr.sprite = AIAssistantWorry;
		if(playerHeat > 150)
			sr.sprite = AIAssistantDispleased;
		if(playerHeat > 200)
			sr.sprite = AIAssistantDesperate;
		if(playerHeat < 30)
			sr.sprite = AIAssistantAmazed;
	}
}
